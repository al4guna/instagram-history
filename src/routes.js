import React from 'react'
import { BrowserRouter, Switch, Route  } from 'react-router-dom'

import { Home } from './components/Home/'

import { GlobalStyle } from './styled/GlobalStyled' 

export const Router = () => {
    return (
        <BrowserRouter>
            <GlobalStyle />
            <Switch>
                <Route path="/"> <Home /> </Route>
                <Route path="/history/:name"> </Route>
            </Switch>
        </BrowserRouter>
    )
}
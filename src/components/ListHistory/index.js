import React from 'react'

import { History } from '../History'
import { List } from './styled'

export const ListHistory = () => {
    return (
        <List>
        {
            [1,2,3,4,5,6,7,8,9].map((item, key) => {
                return <History key={key}/>
            })
        }
        </List>

    )
}
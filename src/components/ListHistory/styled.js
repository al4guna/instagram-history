import styled from 'styled-components'

export const List = styled.div`
    background: #Fafafa;
    width: 100%;
    padding 10px 0px 10px 10px;
    display: flex;
    overflow-x: scroll;
    border-top: 1px solid #dbdbdb;
    border-bottom: 1px solid #dbdbdb;
    ::-webkit-scrollbar {
        display: none;
    }
`
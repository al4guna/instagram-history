import styled from 'styled-components'

export const Item = styled.div`
    width: 70px;
    text-align: center;
    margin: 0px 7px;
`

export const Circle = styled.div`
    display: flex;
    justify-content: center;
    align-items: center;
    border-radius: 50%;
    width: 68px;
    height: 68px;
    margin-bottom: 2px;
    background: -moz-linear-gradient(left,rgba(51,68,131,1) 0%,rgba(110,35,104,1) 100%);
    background: -webkit-gradient(left top,right top,color-stop(0%,rgba(51,68,131,1)),color-stop(100%,rgba(110,35,104,1)));
    background: -webkit-linear-gradient(left,rgba(51,68,131,1) 0%,rgba(110,35,104,1) 100%);
    background: -o-linear-gradient(left,rgba(51,68,131,1) 0%,rgba(110,35,104,1) 100%);
    background: -ms-linear-gradient(left,rgba(51,68,131,1) 0%,rgba(110,35,104,1) 100%);
    background: linear-gradient(to right,rgba(51,68,131,1) 0%,rgba(110,35,104,1) 100%);
`
export const Border = styled.div`
    box-sizing: border-box;
    background: transparent;
    display: flex;
    justify-content: center;
    align-items: center;
    height: 95%;
    width: 95%;
    border-radius: 50%;
    overflow: hidden;
    border: 2px solid white;
`

export const Image = styled.img`
    width: 100%;
    box-sizing: inherit;
`

export const Text = styled.p`
    font-family: 'Roboto', sans-serif;
    color: #121A2F;
    display: block;
    font-size: 10px;
    font-weight: 400;
    width: 100%;
    white-space: nowrap;
    text-overflow: ellipsis;
    overflow: hidden;
    margin: 0;
`

import React from 'react'

import { Item, Circle, Border, Image, Text } from './styled'

const URL_IMAGE = "https://cdn.pixabay.com/photo/2017/12/27/10/57/stockings-and-garters-3042410_960_720.jpg"
export const History = ({image = URL_IMAGE, name = "Motel Luna Roja"}) => {
    return (
        <Item>
            <Circle>
                <Border>
                    <Image src={image}/>
                </Border>
            </Circle>
            <Text> { name }</Text>
        </Item>
    )
}